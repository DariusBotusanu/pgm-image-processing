var searchData=
[
  ['point_21',['Point',['../classmy_namespace_1_1_point.html',1,'myNamespace::Point'],['../classmy_namespace_1_1_point.html#a434f6ccf51073c34d58765dc858b6411',1,'myNamespace::Point::Point()'],['../classmy_namespace_1_1_point.html#a7503c824452a5d06c3196e9bc4e1c15e',1,'myNamespace::Point::Point(unsigned int x, unsigned int y)']]],
  ['process_22',['process',['../classmy_namespace_1_1_brightness_contrast.html#aafc0043cb908bc1db4fb27987bd41136',1,'myNamespace::BrightnessContrast::process()'],['../classmy_namespace_1_1_gamma_correction.html#a01c848183a2cc582cad437de48f5b006',1,'myNamespace::GammaCorrection::process()'],['../classmy_namespace_1_1_image_processing.html#a88fc7e1f5db5d01d6e11e14ef2665e56',1,'myNamespace::ImageProcessing::process()']]]
];
