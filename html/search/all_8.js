var searchData=
[
  ['readkernel_23',['readKernel',['../classmy_namespace_1_1_convolution.html#a5f41ee170f91b390ca79f833f66f2d96',1,'myNamespace::Convolution']]],
  ['rectangle_24',['Rectangle',['../classmy_namespace_1_1_rectangle.html',1,'myNamespace::Rectangle'],['../classmy_namespace_1_1_rectangle.html#ac85af0b7cb2a81caea08dc0be59f0ef9',1,'myNamespace::Rectangle::Rectangle()'],['../classmy_namespace_1_1_rectangle.html#a9b4b08e0a24e7c53a4be82c8eadb3b25',1,'myNamespace::Rectangle::Rectangle(Point topLeft, unsigned int width, unsigned int height)'],['../classmy_namespace_1_1_rectangle.html#a45f1f354d8e78c51a828fa23f39a9492',1,'myNamespace::Rectangle::Rectangle(Point topLeft, Point bottomRight)']]],
  ['release_25',['release',['../classmy_namespace_1_1_convolution.html#a454ca9919441ed7b1883cf3614720e40',1,'myNamespace::Convolution::release()'],['../classmy_namespace_1_1_image.html#a51af79a9a8b64c80c31a6b9ebb4de8f9',1,'myNamespace::Image::release()'],['../classmy_namespace_1_1_rectangle.html#a5cfca6ebf88dcea0a6c33553df71d118',1,'myNamespace::Rectangle::release()']]]
];
